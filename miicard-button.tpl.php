<?php
/**
 * @file
 * Provides a button to sign in or verify your identity with miiCard.
 *
 * Available variables:
 * - $path
 *      Path to navigate to to start the login or authorisation process
 * - $text
 *      Configured text for the button as per the miiCard admin panel
 */
?>
<a href="<?php print $path; ?>" <?php print drupal_attributes($link_attributes); ?>>
  <?php print $text; ?>
</a>
